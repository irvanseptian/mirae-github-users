function findFirstCharAppearingThreeTimes(arr) {
  const charCount = {};

  for (let i = 0; i < arr.length; i++) {
      const char = arr[i];

      if (charCount[char]) {
          charCount[char]++;
      } else {
          charCount[char] = 1;
      }

      if (charCount[char] === 3) {
          console.log(`First character appearing three times: ${char}`);
          return char;
      }
  }

  return null;
}

export const threeTimes = () => {
  const randomChars = ['a', 'b', 'c', 'a', 'd', 'e', 'c', 'f', 'g', 'a'];
  const result = findFirstCharAppearingThreeTimes(randomChars);

  if (result === null) {
    console.log("No character appears three times in the array.");
  }
}





