export default function createReducer(initialState, fnMap) {
  return (state = initialState, { type, payload, meta }) => {
    const handle = fnMap[type]
    return handle ? handle(state, payload, meta) : state
  }
}
