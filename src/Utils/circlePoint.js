function findCenter(points, targetPoint) {
  if (points.length === 0) {
      return { x: 0, y: 0 };
  }

  let sumX = 0;
  let sumY = 0;
  for (let i = 0; i < points.length; i++) {
      sumX += points[i].x;
      sumY += points[i].y;
  }

  const centerX = sumX / points.length;
  const centerY = sumY / points.length;

  const center = { x: centerX, y: centerY };

  if (center.x === targetPoint.x && center.y === targetPoint.y) {
      console.log("The point is the center.");
  } else {
      console.log("The point is not the center.");
  }

  return center;
}

const points = [
  { x: 1, y: 2 },
  { x: 3, y: 4 },
  { x: 5, y: 6 }
];

const targetPoint = { x: 3, y: 4 }; // Change this to the point you want to check
export const center = findCenter(points, targetPoint);
