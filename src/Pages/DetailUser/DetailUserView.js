// COMPONENTS
import SearchBar from "../../Components/Reuseable/SearchBar"
import EmptyData from "../../Components/Reuseable/EmptyData"
import Loading from "../../Components/Reuseable/Loading"

import RepoCard from "./Components/RepoCard"

// STYLES
import "./styles.scss"

function DetailUserView({
  keyword,
  data,
  isLoading,
  setKeyword,
  handleSubmitSearch,
}) {
  return (
    <>
      <SearchBar
        value={keyword}
        onChange={setKeyword}
        onClick={handleSubmitSearch}
        placeholder="Search User"
      />
      <div className="detail-user">
        {isLoading && (
          <Loading />
        )}
        {!isLoading && data.length === 0 ? (
          <EmptyData />
        ) : (
          data.map((item, index) => (
            <RepoCard
              key={index}
              image={item.owner.avatar_url}
              name={item.name}
              description={item.description}
              owner={item.owner.login}
              visibility={item.visibility}
            />
          ))
        )}
      </div>
    </>
  )
}

export default DetailUserView
