// CORE
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"

// REDUX
import { userListsFetch } from "../../ReduxStore/Ducks/UserLists"

// TEST FUNCTIONAL LOGIC TEST
import { center } from "../../Utils/circlePoint";
import { threeTimes } from "../../Utils/character3Times";

// VIEW COMPONENT
import SearchUserView from "./SearchUserView"

const SearchUserContainer = () => {
  const dispatch = useDispatch();
  const state = useSelector((state) => state.UserLists)

  const [keyword, setKeyword] = useState("")

  // TESTING FUNCTIONAL LOGIC TEST
  useEffect(() => {
    // eslint-disable-next-line no-unused-expressions
    center;
    threeTimes();
  }, [])

  useEffect(() => {
    dispatch(userListsFetch({ keyword: "irvanseptian0809" }))
  },[dispatch])

  const handleSubmitSearch = () => {
    dispatch(userListsFetch({ keyword }))
  }

  const handleLoadMore = () => {
    dispatch(userListsFetch({
      keyword,
      page: state.pagination.page + 1,
    }))
  }

  const props = {
    keyword,
    data: state.data,
    total: state.pagination.total,
    isLoading: state.isLoading,
    setKeyword,
    handleSubmitSearch,
    handleLoadMore,
  }

  return <SearchUserView {...props} />
}

export default SearchUserContainer