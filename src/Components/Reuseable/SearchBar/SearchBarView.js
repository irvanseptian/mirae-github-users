// COMPONENT
import Input from "../../Basic/Input"
import Button from "../../Basic/Button"
import Divider from "../../Basic/Divider"

// STYLES
import "./styles.scss"

const SearchBarView = ({
  onChange,
  onClick,
  value,
  placeholder,
}) => {
  return (
    <>
      <div className="search-bar-component">
        <div>
          <Input
            value={value}
            placeholder={placeholder}
            onChange={onChange}
          />
        </div>
        <div>
          <Button onClick={onClick}>
            Search
          </Button>
        </div>
      </div>
      <Divider className="mt-20 mb-20"/>
    </>
  )
}

export default SearchBarView