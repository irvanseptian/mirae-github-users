import "./styles.scss"

const InputView = ({
  onChange,
  value,
  placeholder,
}) => {
  return (
    <input
      value={value}
      onChange={(e) => onChange(e.target.value)}
      className="input-component"
      placeholder={placeholder}
    />
  )
}

export default InputView