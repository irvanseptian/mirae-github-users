import "./styles.scss"

const InputView = ({
  onClick,
  children,
}) => {
  return (
    <button
      onClick={onClick}
      className="button-component"
    >
      {children}
    </button>
  )
}

export default InputView