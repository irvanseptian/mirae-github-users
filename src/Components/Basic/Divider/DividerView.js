// STYLES
import "./styles.scss"

const DividerView = ({
  className,
}) => {
  return (
    <div className={`divider-component ${className}`}/>
  )
}

export default DividerView
