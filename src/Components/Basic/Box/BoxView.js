// STYLES
import "./styles.scss"

const SearchBarView = ({
  children,
  className,
}) => {
  return (
    <div className={`box-component ${className}`}>
      {children}
    </div>
  )
}

export default SearchBarView