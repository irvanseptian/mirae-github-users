// STYLES
import "./styles.scss"

const LabelView = ({
  children,
  type = "default",
  className,
}) => {
  return (
    <label className={`label-${type} ${className}`}>{children}</label>
  )
}

export default LabelView