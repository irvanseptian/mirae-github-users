import { combineReducers } from 'redux'
import UserLists from '../Ducks/UserLists'
import UserDetails from '../Ducks/UserDetails'

const appReducers = combineReducers({
  UserLists,
  UserDetails,
})

const rootReducers = (state, action) => {
  return appReducers(state, action)
}

export default rootReducers