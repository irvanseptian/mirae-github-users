import { combineEpics } from 'redux-observable'

import * as UserListsEpics from '../Epics/UserLists'
import * as UserDetailsEpics from '../Epics/UserDetails'
import api from '../../Utils/api'

function rootEpics(action$, store) {
  const dependencies = {
    api,
  }
  const allEpics = [
    ...Object.values(UserListsEpics),
    ...Object.values(UserDetailsEpics),
  ]

  return combineEpics(...allEpics)(action$, store, dependencies)
}

export default rootEpics