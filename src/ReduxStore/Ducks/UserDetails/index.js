import createReducer from '../../../Utils/createReducer'

export const USER_DETAILS = 'USER_DETAILS'
export const USER_DETAILS_SUCCESS = 'USER_DETAILS_SUCCESS'
export const USER_DETAILS_FAILED = 'USER_DETAILS_FAILED'

export const INITIAL_STATE = {
  data: [],
  login: "",
  isLoading: false,
  isError: false,
  errorMessage: '',
}

const reducer = createReducer(INITIAL_STATE, {
  [USER_DETAILS]: (state, payload) => ({
    ...state,
    isLoading: true,
    login: payload,
  }),
  [USER_DETAILS_SUCCESS]: (state, payload) => ({
    ...state,
    isLoading: false,
    isError: false,
    data: payload,
  }),
  [USER_DETAILS_FAILED]: (state, payload) => ({
    ...state,
    isLoading: false,
    isError: true,
    errorMessage: payload,
  })
})

export const userDetailsFetch = (payload) => ({
  type: USER_DETAILS,
  payload,
})
export const userDetailsFetchSuccess = (payload) => ({
  type: USER_DETAILS_SUCCESS,
  payload,
})
export const userDetailsFetchFailed = (payload) => ({
  type: USER_DETAILS_FAILED,
  payload,
})

export default reducer